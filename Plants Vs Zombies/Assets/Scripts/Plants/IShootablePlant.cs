﻿using System.Collections;
using UnityEngine;
[System.Serializable]
public class GunPlantInfo{

	public Bullet bullet;
	public float fireRate;
	public float bulletRate;

}

public interface IShootablePlant{

	IEnumerator Shoot();
	GunPlantInfo GunPlantStats{get;}

}