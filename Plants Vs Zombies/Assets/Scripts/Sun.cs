﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Sun : MonoBehaviour
{


	[SerializeField]
	private float sunPopInTime;

	[SerializeField]
	private float sunPopOutTime;

	[SerializeField]
	private float dissapearTime;

	[SerializeField]
	private SpriteRenderer spriteRenderer;


	private void SpawnSun ()
	{
	
		spriteRenderer.DOFade (0.7f, 1f).SetLoops (-1);


	}

	private void Start(){
		SpawnSun ();
		Destroy (this.gameObject, dissapearTime);
	}

}
