﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class GameConstants{

	public int SunReward = 5;
	public float SpawnTime = 5f;

}

public class GameManager : MonoBehaviour {


	private static GameManager _instance;

	public static GameManager Instance{

		get{

			if (_instance == null) {

				_instance = FindObjectOfType<GameManager> ();

				if (_instance == null) {

					GameObject gameManager = new GameObject ("Game Manager");
					gameManager.AddComponent<GameManager> ();

					_instance = gameManager.GetComponent<GameManager> ();
				}
			}

			return _instance;

		}

	}

	private void Start(){

		

	}

	public GameConstants gameConstants;

	public int currentSunflowers = 0;

	public PlantTranslator selectedPlant = null;

	[SerializeField]
	private List<PlantTranslator> purchasableFlowers;

	[SerializeField]
	private LayerMask uiLayer;

	public void CollectSun(Sun sun){ //TODO:: MAKE AN EVENT FOR PLANTTRASNFER DIMMING

		currentSunflowers += gameConstants.SunReward;
		Destroy (sun.gameObject);
		UIManager.RefreshUI ();
	}

	private void ProcessRay(RaycastHit2D hit){

		if (hit.transform.GetComponent<Tile> () != null && hit.transform.GetComponent<Tile>().currentPlant == null && selectedPlant != null) {
			
			selectedPlant.OnSuccesfulPlant (TileManager.Instance.GetVectorByCoordinates (hit.transform.GetComponent<Tile> ().Coordinates), hit.transform.GetComponent<Tile>());

			return;
		}
		if (hit.transform.GetComponent<Sun>() != null) {

			CollectSun (hit.transform.GetComponent<Sun>());
			return;

		}


	}

	public void DoRay(){ 

		Camera mainCamera = Camera.main;
		Ray ray = mainCamera.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction,uiLayer);

		if (hit.transform)
			ProcessRay (hit);
		else if (selectedPlant != null && selectedPlant.isPreparingPlant) {
		//	selectedPlant.DeselectPlant (selectedPlant);
		//	selectedPlant.isPreparingPlant = false;
		//	selectedPlant = null;
		//	Debug.Log ("Clicked somwhere else");

		}
	}




	private void Update(){

		if (Input.GetMouseButtonDown (0)) {

			DoRay ();

		}

	}


}
