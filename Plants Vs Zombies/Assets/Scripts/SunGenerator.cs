﻿using UnityEngine;
using System.Collections;

public class SunGenerator : MonoBehaviour {

	[SerializeField]
	private float minSpawnTime;

	[SerializeField]
	private float maxSpawnTime;

	[SerializeField]
	private Sun sun;

	[SerializeField]
	private float fallSpeed;

	private IEnumerator StartSpawning(){
		while (true) {
			
			float ranTime = Random.Range (minSpawnTime, maxSpawnTime);
			yield return new WaitForSeconds (ranTime);
			SpawnSun ();

		}
	}

	private IEnumerator Fall(GameObject obj){

		while (true) {
			if (obj != null) {
				obj.transform.Translate (Vector2.down * fallSpeed * Time.deltaTime);
				yield return null;
			} else {
				yield break;
			}

		}


	}


	private void SpawnSun(){

		int widthPixels = Screen.width;
		float edgePoint = Camera.main.ScreenToWorldPoint (new Vector2 (widthPixels, 0)).x + 1; 
		float topEdgePoint = Camera.main.ScreenToWorldPoint (new Vector2 (0, Screen.height)).y + 2;
		float ranX = Random.Range (-edgePoint, edgePoint);
		GameObject obj = (GameObject)GameObject.Instantiate (sun.gameObject, new Vector3(ranX,topEdgePoint,-9), Quaternion.identity);
		StartCoroutine (Fall (obj));

	}

	private void Start(){

		StartCoroutine (StartSpawning ());

	}


}
