﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;


public class PlantTranslator : MonoBehaviour{

	public PlantInfo plantInfo;

	[SerializeField]
	internal GameObject prefab;

	private Text priceText = null;

	Button button;

	Image sRenderer;


	public bool isPreparingPlant = false;

	private void initializeVars(){

		if (transform.FindChild ("Price") != null)
			priceText = transform.FindChild ("Price").GetComponent<Text> ();

		button = GetComponent<Button> ();
		sRenderer = GetComponent<Image> ();



		OnInitComplete ();
	}

	private void OnInitComplete(){

		priceText.text = plantInfo.purchasePrice.ToString ();

	}

	public virtual void OnStart(){

	}
		
	private void Spawn(Vector2 pos, Tile tile){

		GameObject obj = (GameObject)GameObject.Instantiate (prefab,pos,Quaternion.identity);
		tile.currentPlant = obj.GetComponent<Plant> ();
		obj.GetComponent<Plant> ().Initialize (plantInfo);
		OnStart ();


	}

	private void BeginRefill(){

		button = GetComponent<Button> ();
		sRenderer= GetComponent<Image> ();
		button.interactable = false;
		button.image.color = new Color (1, 1, 1, 0);
		sRenderer.DOFade(1,plantInfo.refilTime)
			.OnComplete(()=> button.interactable = true);


	}

	public void OnSuccesfulPlant(Vector2 position, Tile tile){
		GameManager gm = GameManager.Instance;
		TileManager tileManager = TileManager.Instance;
		Spawn (position,tile);
		gm.selectedPlant = null;
		BeginRefill ();
		gm.currentSunflowers -= plantInfo.purchasePrice;
		UIManager.RefreshUI ();
	}

	private void ChangeSelectedPlant(){
		GameManager gm = GameManager.Instance;
		gm.selectedPlant.sRenderer.color = new Color (1, 1, 1, 1);
		sRenderer.color = new Color (1, 1, 1, 0.5f);
		gm.selectedPlant = this;
		gm.selectedPlant.isPreparingPlant = true;


	}

	public void DeselectPlant(PlantTranslator plant){

		plant.sRenderer.color = new Color (1, 1, 1, 1);

	}

	public void TrySelect(){

		GameManager gm = GameManager.Instance;



		if (gm.selectedPlant == this) {

			button.image.color = new Color (1, 1, 1, 1);
			gm.selectedPlant.isPreparingPlant = false;
			gm.selectedPlant = null;
			return;
		}

		if (gm.currentSunflowers >= plantInfo.purchasePrice) {

			if (gm.selectedPlant != null) {

				ChangeSelectedPlant ();
				return;

			}

			button.image.color = new Color (1, 1, 1, 0.5f);
			gm.selectedPlant = this;
			gm.selectedPlant.isPreparingPlant = true;


		}
	}

	private void Start(){

	
		initializeVars ();



	}



}


[System.Serializable]
public class PlantInfo{

	public int purchasePrice;
	public int health;
	public float refilTime;
	

	public PlantInfo(int purchasePrice){

		this.purchasePrice = purchasePrice;
		refilTime = 1f;
		health = 100;

	}

	public PlantInfo(){

		purchasePrice = 1;
		refilTime = 1f;
		health = 100;
	}
}